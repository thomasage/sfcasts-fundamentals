<?php
declare(strict_types=1);

namespace App\Service;

use Psr\Cache\CacheItemInterface;
use Symfony\Bridge\Twig\Command\DebugCommand;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\DependencyInjection\Attribute\Autowire;
use Symfony\Contracts\Cache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class MixRepository
{
    public function __construct(
        private readonly HttpClientInterface $githubContentClient,
        private readonly CacheInterface $cache,
        #[Autowire('%kernel.debug%')]
        private readonly bool $isDebug,
        #[Autowire(service: 'twig.command.debug')]
        private readonly DebugCommand $twigDebugCommand,
    ) {
    }

    public function findAll(): array
    {
//        $output = new BufferedOutput();
//        $this->twigDebugCommand->run(new ArrayInput([]), $output);
//        dd($output);

        return $this->cache->get('mixes_data', function (CacheItemInterface $cacheItem): array {
            $cacheItem->expiresAfter($this->isDebug ? 5 : 60);
            $response = $this->githubContentClient->request(
                'GET',
                '/SymfonyCasts/vinyl-mixes/main/mixes.json'
            );

            return $response->toArray();
        });
    }
}
